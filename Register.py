import pymssql
import tkinter as tk
from tkinter import messagebox

#数据库连接
conn = pymssql.connect(server='127.0.0.1',user='sa',password='ASDasd123',database='KS_python')
if conn:
    print("数据库连接成功！")
else:
    print("数据库连接失败！")
#建立游标
cur = conn.cursor()

root = tk.Tk()
root.geometry('300x180')
root.title('Register')

R_Username = tk.StringVar()
R_password = tk.StringVar()

page = tk.Frame(root)
page.pack()
tk.Label(page).grid(row=0,column=0)

tk.Label(page,text='用户名：').grid(row=1,column=1)
tk.Entry(page,textvariable=R_Username).grid(row=1,column=2)
tk.Label(page,text='密码').grid(row=2,column=1,pady=10)
tk.Entry(page,textvariable=R_password).grid(row=2,column=2)

def Register():
    R_name = (R_Username.get())
    R_pwd = (R_password.get())
    print(R_name,R_pwd)
    cur.execute('select * from Admin where username=%s',R_name)
    ret = cur.fetchall()
    if not ret:
        cur.execute('insert into Admin values(%s,%s)',(R_name,R_pwd))
        conn.commit()
        messagebox.showinfo(title="注册成功！", message="注册成功！")
    else:
        messagebox.showerror(title="注册失败",message="用户名已存在！")

tk.Button(page,text='注册',command=Register).grid(row=3,column=1,pady=10)
tk.Button(page,text='取消',command=page.quit).grid(row=3,column=2)

root.mainloop()
