import tkinter as tk
from tkinter import messagebox
import pymssql

#数据库连接 user为数据库用户名  password为数据库密码  database为数据库名
con = pymssql.connect(server='127.0.0.1', user='sa', password='ASDasd123', database='KS_python')
#创建数据库游标 对数据库进行操作
cur = con.cursor()

#创建登录窗口
root = tk.Tk()
root.geometry('300x180')
root.title('Login')

#用户登录信息
Username = tk.StringVar()
password = tk.StringVar()

#创建页面
page = tk.Frame(root)
page.pack()

tk.Label(page).grid(row=0,column=0)

#建立用户名输入框
tk.Label(page,text='账户：').grid(row=1,column=1)
tk.Entry(page,textvariable=Username).grid(row=1,column=2)

#建立密码输入框
tk.Label(page,text='密码：').grid(row=2,column=1,pady=10)
tk.Entry(page,textvariable=password).grid(row=2,column=2)

#登录校验
def Login():
    name = str(Username.get())
    pwd = str(password.get())
    print(name, pwd)
    #发送数据库语句
    cur.execute('select * from Admin where username=%s and password=%s', (name, pwd))
    #接受数据库返回结果
    sql_login_result =cur.fetchall()
    #判断登陆情况
    if sql_login_result:
        messagebox.showinfo(title="登录成功！",message="登陆成功！")
    else:
        messagebox.showerror(title="警告",message="登陆失败！")

#登录按钮
tk.Button(page,text='登录',command=Login).grid(row=3,column=1,pady=10)
tk.Button(page,text='取消',command=page.quit).grid(row=3,column=2)

#监控窗口
root.mainloop()
#关闭游标
con.close()