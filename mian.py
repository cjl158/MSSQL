import datetime
import pymssql
import ttkbootstrap
import tkinter as tk
from tkinter import messagebox
Admin = 0
Sno_global = ''
Sname_global = ''
Aid_global = ''
Aname_global = ''
Change_result = 0
startime = 0
URoom = 0
FreeRoom = 0
DeletG = 0


#连接数据库
#user 和 password改为自己的信息
con = pymssql.connect(host='101.42.154.59', user='sa', password='123456', database='MSSQL')
cur = con.cursor()
cur1 = con.cursor()
cur2 = con.cursor()
cur3 = con.cursor()
cur4 = con.cursor()

# #全局变量测试代码
# def text():
#     print('当前全局变量为Sno_global='+Sno_global)
#     print("Sname_global="+Sname_global)

#获取当前登录用户的姓名并赋值全局变量
def GetSname():
    cur.execute('SELECT Sname FROM StudentInfo WHERE Sno = %s',Sno_global)
    results = cur.fetchall()
    for result in results:
        result = list(result)  # 元组转化为列表
        for res in range(len(result)):
            if isinstance(result[res], str):
                result[res] = result[res].replace(' ', '')  # 解决空格问题
        result = tuple(result)  # 列表再.转换为元组
    global Sname_global
    Sname_global = result[0]

# 定义一个方法，用于判断是否为中文字符
def is_Chinese(ch):
    if ch >= '\u4e00' and ch <= '\u9fa5':
        return True
    else:
        return False

# 定义一个方法，用于计算中西文混合字符串的字符串长度
def Len_Str(string):
    count = 0
    for line in string:
        if is_Chinese(line):
            count = count + 2
        else:
            count = count + 1
    return count

#校验管理员登陆信息
def check_Admin(UserName, PassWord):
    id = str(UserName.get())
    pwd = str(PassWord.get())
    cur.execute('select * from Admin where id=%s and password=%s', (id, pwd))
    sql_login_result = cur.fetchall()
    if sql_login_result:
        messagebox.showinfo(title='提示', message='登陆成功！')
        cur1.execute('select * from Admin where id=%s',id)
        results = cur1.fetchall()
        for result1 in results:
            result1 = list(result1)  # 元组转化为列表
            for res in range(len(result1)):
                if isinstance(result1[res], str):
                    result1[res] = result1[res].replace(' ', '')  # 解决空格问题
            result1 = tuple(result1)  # 列表再.转换为元组
        name = result1[0]
        global Admin,Aid_global,Aname_global
        Admin = 1
        Aid_global = id
        Aname_global = name
        root.destroy()
    else:
        messagebox.showerror(title='警告', message='登陆失败！')

#校验用户登录信息
def check_User(Sno,Spwd):
    name = str(Sno.get())
    pwd = str(Spwd.get())
    cur.execute('select * from StudentInfo where Sno=%s and Spwd=%s', (name, pwd))
    sql_login_result = cur.fetchall()
    # cur1.execute('select Sname from StudentInfo where Sno=%s',name)
    # Sname = list(cur1.fetchall())
    if sql_login_result:
        messagebox.showinfo(title='提示', message='登陆成功！')
        global Sno_global
        Sno_global = name
        GetSname()
        login.destroy()
        Temp()
    else:
        messagebox.showerror(title='警告', message='用户名或密码错误！')

def Temp_warn(date,Stemp):
    count = 0  #记录体温异常表中信息个数
    #获取体温异常表编号
    cur.execute('SELECT No FROM Temp')
    results = cur.fetchall()
    for i in results:
        count += 1
    #执行sql语句
    cur.execute('insert into Temp(No,Sno,Stemp,Sdate)values(%s,%s,%s,%s)', (count+1, Sno_global,Stemp,date))
    con.commit()

def Temp_add(temp1,date1):
    global temp_add_result
    temp1 = float(temp1.get())
    date1 = str(date1.get())
    sno = Sno_global
    attention = 0
    if temp1>37.3 and temp1<42:
        attention = 1
        Temp_warn(date1,temp1)
        messagebox.showerror(title='警告',message='体温异常！已自动报备！')
        temp.destroy()
    if temp1>42 or temp1<35:
        attention = 2
        messagebox.showerror(title='警告',message='体温填写错误，请修改后重新报备！')
    if attention == 0:
        cur.execute('UPDATE StudentInfo SET Stemp = %s WHERE Sno = %s', (temp1,sno))
        con.commit()
        messagebox.showinfo(title='提示',message='体温报备成功！')
        temp.destroy()
        SelectRoomWindows()

def SelectRoomWindows():
    srw = tk.Toplevel(root)
    srw.title('自习室选择')
    srw.geometry('250x120')
    select = tk.StringVar()
    tk.Label(srw,text='自习室(1-3)：',width=15).place(x=10,y=20)
    tk.Entry(srw,textvariable=select,width=15).place(x=100,y=20)
    tk.Button(srw,text='登记',command=lambda :[SelectRoom(select,srw)]).place(x=100,y=70,anchor='nw')

def SelectRoom(room,srw):
    no = str(room.get())
    if no =='1' or no =='2' or no == '3':
        cur.execute('UPDATE StudentInfo SET Rno = %s where Sno = %s',(no,Sno_global))
        cur1.execute('SELECT * FROM Room where Rno = %s',no)
        results = cur1.fetchall()
        for result1 in results:
            result1 = list(result1)  # 元组转化为列表
            for res in range(len(result1)):
                if isinstance(result1[res], str):
                    result1[res] = result1[res].replace(' ', '')  # 解决空格问题
            result1 = tuple(result1)  # 列表再.转换为元组
        free_sql = result1[1]
        free_sql -= 1
        global FreeRoom
        FreeRoom = free_sql
        if FreeRoom >0:
            cur2.execute('UPDATE Room SET Rfree = %s where Rno = %s',(free_sql,no))
            con.commit()
            messagebox.showinfo(title='成功',message='进入成功！可容纳人数为：'+str(FreeRoom))
            global URoom
            URoom = no
            srw.destroy()
            GoodsFWindows_U()
        else:
            messagebox.showerror(title='错误',message='该自习室已满，请重新选择！')
    else:
        messagebox.showerror(title='警告', message='请输入1-3自习室')



def Temp():
    global temp
    temp = tk.Toplevel(root)
    temp.title('体温报备')
    temp.geometry('300x150')
    t = tk.StringVar()
    date = tk.StringVar()
    tk.Label(temp,text='体 温：',width=15).place(x=10,y=20)
    tk.Entry(temp,textvariable=t,width=15).place(x=100,y=20)
    tk.Label(temp,text='日 期',width=15).place(x=10,y=60)
    tk.Entry(temp,textvariable=date,width=15).place(x=100,y=60)
    tk.Button(temp,text='登记',command=lambda :[Temp_add(t,date)]).place(x=120,y=100,anchor='nw')

def GoodsFWindows_U():
    Goods = tk.Toplevel(root)
    Goods.title('损坏物品报备')
    Goods.geometry('200x150')
    global Admin
    Admin = 2
    tk.Label(Goods,text='损坏物品报备系统',width=15).place(x=20,y=20)
    tk.Button(Goods,text='无损坏',command=lambda :[root.destroy()]).place(x=10,y=50)
    tk.Button(Goods,text='有损坏',command=lambda :[GoodsReportWindows_U()]).place(x=80,y=50)

def GoodsReportWindows():
    Goods = tk.Toplevel(root2)
    goods_id = tk.StringVar()
    Goods.title('损坏物品报备')
    Goods.geometry('230x150')
    tk.Label(Goods,text='请输入物品编号查询存在后再报备！').place(x=10,y=15)
    tk.Label(Goods,text='物品编号：').place(x=10,y=50)
    tk.Entry(Goods,textvariable=goods_id,width=15).place(x=80,y=50)
    tk.Button(Goods,text='报备',command=lambda :[GoodsReport(goods_id,Goods)]).place(x=60,y=85)

def GoodsReport(id,windows):
    id = str(id.get())
    cur.execute('select * from Goods where Gno = %s',id)
    results = cur.fetchall()
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace(' ', '')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
    if results:
        GR_U(result1)
        windows.destroy()
    else:
        messagebox.showerror(title='错误',message='所查询物品不存在！')
        windows.destroy()

def GoodsReport_U(id):
    id = str(id.get())
    cur.execute('select * from Goods where Gno = %s',id)
    results = cur.fetchall()
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace(' ', '')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
    if results:
        GR_UY(result1)

    else:
        messagebox.showerror(title='错误',message='所查询物品不存在！')

def GR_UY(r):
    windows = tk.Toplevel(root)
    windows.title('提示窗口')
    windows.geometry('220x180')
    tk.Label(windows,text='查询物品信息如下：\n').place(x=20,y=10)
    tk.Label(windows,text='物品编号:'+str(r[0]),).place(x=20,y=35)
    tk.Label(windows,text='物品名称:'+str(r[1])).place(x=20,y=60)
    tk.Label(windows,text='物品所属自习室:'+str(r[2])).place(x=20,y=85)
    tk.Label(windows,text='-------------------------------------').place(x=0,y=110)
    tk.Button(windows,text='确认',command=lambda :GR_GO(str(r[0]))).place(x=15,y=130)
    tk.Button(windows,text='取消',command=lambda :windows.destroy()).place(x=130,y=130)

def GR_U(r):
    windows = tk.Toplevel(root2)
    windows.title('提示窗口')
    windows.geometry('220x180')
    tk.Label(windows,text='查询物品信息如下：\n').place(x=20,y=10)
    tk.Label(windows,text='物品编号:'+str(r[0]),).place(x=20,y=35)
    tk.Label(windows,text='物品名称:'+str(r[1])).place(x=20,y=60)
    tk.Label(windows,text='物品所属自习室:'+str(r[2])).place(x=20,y=85)
    tk.Label(windows,text='-------------------------------------').place(x=0,y=110)
    tk.Button(windows,text='确认',command=lambda :GR_GO_1(str(r[0]),windows)).place(x=15,y=130)
    tk.Button(windows,text='取消',command=lambda :windows.destroy()).place(x=130,y=130)

def GR_GO(id):
    r = CheckRepair(id)
    if r == 1:
        state = "异常"
        cur.execute('insert into Repair(Gno,Sno,state)values(%s,%s,%s)', (id,Sno_global,str(state)))
        con.commit()
        messagebox.showinfo(title='成功！',message='上报成功！')
    else:
        messagebox.showerror(title='错误', message='该物品已被上报')
    root.destroy()

def GR_GO_1(id,windows):
    r = CheckRepair(id)
    if r == 1:
        state = "异常"
        cur.execute('insert into Repair(Gno,Sno,state)values(%s,%s,%s)', (id,Sno_global,str(state)))
        con.commit()
        messagebox.showinfo(title='成功！',message='上报成功！')
    else:
        messagebox.showerror(title='错误',message='该物品已被上报')
    windows.destroy()

def CheckRepair(id):
    cur.execute('SELECT * FROM Repair where Gno = %s',id)
    r = cur.fetchall()
    if r :
        return 0
    else:
        return 1

def GoodsReportWindows_U():
    Goods = tk.Toplevel(root)
    goods_id = tk.StringVar()
    Goods.title('损坏物品报备')
    Goods.geometry('230x150')
    tk.Label(Goods,text='请输入物品编号查询存在后再报备！').place(x=10,y=15)
    tk.Label(Goods,text='物品编号：').place(x=10,y=50)
    tk.Entry(Goods,textvariable=goods_id,width=15).place(x=80,y=50)
    tk.Button(Goods,text='报备',command=lambda :[GoodsReport_U(goods_id)]).place(x=40,y=85)

def GoodsAdd(id,name,no,Goods):
    id = str(id.get())
    name = str(name.get())
    no = str(no.get())
    str_out = ""
    cur1.execute('insert into Goods(Gno,Gname,Rno)values(%s,%s,%s)', (id,name,no))
    con.commit()
    messagebox.showinfo(title='成功',message='增添物品成功！')
    str_out += "保存物品信息成功！\n"
    str_out += "保存物品信息如下\n"
    str_out += "物品编号："+id+"\n"
    str_out += "物品名称："+name+"\n"
    str_out += "所属自习室编号："+no+"\n"
    result.set(str_out)
    Goods.destroy()

def ChekGoods(id,name,no,Goods):
    GGno = str(id.get())
    cur.execute('SELECT * FROM Goods where Gno = %s',GGno)
    results = cur.fetchall()
    if results:
        messagebox.showerror(title='提示', message='物品已存在！')
    else:
        GoodsAdd(id, name, no, Goods)

def GoodsWinidows():
    Goods = tk.Toplevel(root1)
    goods_id = tk.StringVar()
    goods_name = tk.StringVar()
    goods_no = tk.StringVar()
    Goods.title('物品增添')
    Goods.geometry('200x200')
    tk.Label(Goods,text='物品编号',width=15).place(x=10,y=20)
    tk.Entry(Goods,textvariable=goods_id,width=15).place(x=100,y=20)
    tk.Label(Goods,text='物品名称',width=15).place(x=10,y=50)
    tk.Entry(Goods,textvariable=goods_name,width=15).place(x=100,y=50)
    tk.Label(Goods,text='所属自习室',width=15).place(x=10,y=80)
    tk.Entry(Goods,textvariable=goods_no,width=15).place(x=100,y=80)
    tk.Button(Goods,text='确定',command=lambda :[ChekGoods(goods_id,goods_name,goods_no,Goods)]).place(x=120,y=100)

def SearchBadGoods():
    str_out = ""
    cur.execute('SELECT * FROM Repair')
    results = cur.fetchall()
    str_out +="损坏物品信息状态如下：\n"
    str_out += (
                "{:^7}".format("物品编号") +
                "{:^7}".format("报修学生学号") +
                "{:^7}".format("管理员工号") +
                "{:^5}".format("状态") +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += (
                "{:^}".format(result1[0]) + ' ' * (11 - Len_Str(result1[0])) +
                "{:^}".format(result1[1]) + ' ' * (
                        13 - Len_Str(result1[1])) +
                "{:^}".format(str(result1[2])) + ' ' * (10 - Len_Str(str(result1[2]))) +
                "{:^}".format(result1[3]) + ' ' * (
                        5 - Len_Str(result1[3])) +
                "\n")
    result.set(str_out)

def ShowAllStudents():
    str_out = ""
    cur.execute('SELECT * FROM StudentInfo')
    results = cur.fetchall()
    str_out +="学生信息如下：\n"
    str_out += "-"*120+"\n"
    str_out += ("{:^}".format("姓名") + ' ' * (12 - Len_Str("姓名")) +
                "{:^}".format("学号") + ' ' * (13 - Len_Str("学号")) +
                "{:^}".format("密码") + ' ' * (13 - Len_Str("密码")) +
                "{:^}".format("性别") + ' ' * (9 - Len_Str("性别")) +
                "{:^}".format("所在自习室") + ' ' * (15 - Len_Str("所在自习室")) +
                "{:^}".format("开始学习时间") + ' ' * (17 - Len_Str("开始学习时间")) +
                "{:^}".format("结束学习时间") + ' ' * (17 - Len_Str("结束学习时间")) +
                "{:^}".format("学习总时间") + ' ' * (13 - Len_Str("学习总时间")) +
                "{:^}".format("体温") + ' ' * (8 - Len_Str("体温")) +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += ("{:^}".format(result1[0]) + ' ' * (10 - Len_Str(result1[0])) +
                "{:^}".format(result1[1]) + ' ' * (14 - Len_Str(result1[1])) +
                "{:^}".format(result1[2]) + ' ' * (13 - Len_Str(result1[2])) +
                "{:^}".format(str(result1[3])) + ' ' * (9 - Len_Str(str(result1[3]))) +
                "{:^}".format(str(result1[4])) + ' ' * (13 - Len_Str(str(result1[4]))) +
                "{:^}".format(str(result1[5])) + ' ' * (18 - Len_Str(str(result1[5]))) +
                "{:^}".format(str(result1[6])) + ' ' * (18 - Len_Str(str(result1[6]))) +
                "{:^}".format(str(result1[7])) + ' ' * (13 - Len_Str(str(result1[7]))) +
                "{:^}".format(str(result1[8])) + ' ' * (7 - Len_Str(str(result1[8]))) +
                "\n")
    result.set(str_out)


def ShowAllGoods():
    str_out = ""
    cur.execute('SELECT * FROM Goods')
    results = cur.fetchall()
    str_out +="自习室物品信息如下：\n"
    str_out += ("{:^}".format("编号") + ' ' * (7 - Len_Str("编号")) +
                "{:^}".format("物品名") + ' ' * (15 - Len_Str("物品名")) +
                "{:^}".format("所属自习室") + ' ' * (
                        11 - Len_Str("所属自习室")) +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += ("{:^}".format(result1[0]) + ' ' * (7 - Len_Str(result1[0])) +
                "{:^}".format(result1[1]) + ' ' * (15 - Len_Str(result1[1])) +
                "{:^}".format(result1[2]) + ' ' * (
                        11 - Len_Str(result1[2])) +
                "\n")
    result.set(str_out)

def RestoreGoods():
    state = str('正常')
    Nstate = str('异常')
    cur.execute('UPDATE Repair SET id = %s where state = %s',(Aid_global,Nstate))
    cur1.execute("UPDATE Repair SET state = %s ", state)
    con.commit()
    messagebox.showinfo(title='成功！',message='成功~！！')

def Add_StudentInfo(Sname,Sno,Spwd,Ssex):
    Sname = str(Sname.get())
    Sno = str(Sno.get())
    Spwd = str(Spwd.get())
    Ssex = str(Ssex.get())
    Ssex_result = 1
    cur.execute('select * from StudentInfo where Sno=%s', Sno)
    Sno_result = cur.fetchall()
    #检测学号重复
    if(Sno_result):
        messagebox.showerror(title='警告',message='所注册学号已存在，请重新输入')
    if Ssex not in "男女":
        messagebox.showerror(title='警告',message='性别输入错误，请输入（男/女）')
        Ssex_result = 0
    if not Sno_result and Ssex_result == 1:
        cur1.execute('insert into StudentInfo(Sno,Sname,Spwd,Ssex)values(%s,%s,%s,%s)', (Sno, Sname, Spwd, Ssex))
        con.commit()
        messagebox.showinfo(title='提示',message='注册成功！')

def Register_User():
    #设置窗口
    regitster = tk.Toplevel(root)
    regitster.title('学生信息注册')
    regitster.geometry('280x210')
    #定义注册信息
    Sname = tk.StringVar()  #学生姓名
    Sno = tk.StringVar()  #学生学号
    Spwd = tk.StringVar()  #学生密码
    Ssex = tk.StringVar()  #学生性别
    #部署姓名
    tk.Label(regitster,text='姓  名',font=('楷体')).place(x=10,y=20)
    tk.Entry(regitster,textvariable=Sname,width=15,font=('楷体')).place(x=100,y=20,anchor='nw')
    #部署学号
    tk.Label(regitster,text='学  号',font=('楷体')).place(x=10,y=55,anchor='nw')
    tk.Entry(regitster,textvariable=Sno,width=15,font=('楷体')).place(x=100,y=55,anchor='nw')
    #部署密码
    tk.Label(regitster,text='密  码',font=('楷体')).place(x=10,y=90,anchor='nw')
    tk.Entry(regitster,textvariable=Spwd,width=15,font=('楷体')).place(x=100,y=90,anchor='nw')
    #部署性别
    tk.Label(regitster,text='性  别',font=('楷体')).place(x=10,y=125,anchor='nw')
    tk.Entry(regitster,textvariable=Ssex,width=15,font=('楷体')).place(x=100,y=125,anchor='nw')
    #部署确认/取消组件
    tk.Button(regitster,text='确认',command=lambda :[Add_StudentInfo(Sname,Sno,Spwd,Ssex),regitster.destroy()],font=('楷体')).place(x=30,y=160,anchor='nw')
    tk.Button(regitster,text='取消',command=lambda :regitster.destroy(),font=('楷体')).place(x=170,y=160,anchor='nw')

def Login_Admin():
    #设置窗口
    login = tk.Toplevel(root)
    login.title('超级管理员登陆')
    login.geometry('280x140')
    #定义用户名、密码变量
    A_UserName = tk.StringVar()
    A_PassWord = tk.StringVar()
    #设置用户名并接收数据
    tk.Label(login,text='用户名',font=('楷体',13)).place(x=20,y=20)
    tk.Entry(login,textvariable=A_UserName).place(x=90,y=20)
    #设置密码并接收数据
    tk.Label(login,text='密码',font=('楷体',13)).place(x=20,y=50)
    tk.Entry(login,textvariable=A_PassWord,show='*').place(x=90,y=50)
    #设置确认按钮
    tk.Button(login,text='登录',command=lambda:check_Admin(A_UserName,A_PassWord),font=('楷体',13)).place(x=60,y=85)
    #设置取消按钮
    tk.Button(login,text='取消',command=lambda :login.destroy(),font=('楷体',13)).place(x=150,y=85)

def Login_User():
    global login
    #设置窗口
    login = tk.Toplevel(root)
    login.title('学生用户登陆')
    login.geometry('280x140')
    #定义用户名、密码变量
    U_UserName = tk.StringVar()
    U_PassWord = tk.StringVar()
    #设置用户名并接收数据
    tk.Label(login,text='用户名',font=('楷体',13)).place(x=20,y=20)
    tk.Entry(login,textvariable=U_UserName).place(x=90,y=20)
    #设置密码并接收数据
    tk.Label(login,text='密码',font=('楷体',13)).place(x=20,y=50)
    tk.Entry(login,textvariable=U_PassWord,show='*').place(x=90,y=50)
    #设置确认按钮
    tk.Button(login,text='登录',command=lambda:check_User(U_UserName,U_PassWord),font=('楷体',13)).place(x=60,y=85)
    #设置取消按钮
    tk.Button(login,text='取消',command=lambda :login.destroy(),font=('楷体',13)).place(x=150,y=85)

def ChangeUser_Windos():
    #设置窗口
    regitster = tk.Toplevel(root1)
    regitster.title('学生信息修改')
    regitster.geometry('300x260')
    #定义注册信息
    Sname = tk.StringVar()  #学生姓名
    Sno = tk.StringVar()  #学生学号
    Spwd = tk.StringVar()  #学生密码
    Ssex = tk.StringVar()  #学生性别
    #提示
    tk.Label(regitster,text='请输入学号查询后再做修改!',font=('楷体',15)).place(x=8,y=15)
    #部署姓名
    tk.Label(regitster,text='姓      名',width=15).place(x=10,y=40)
    tk.Entry(regitster,textvariable=Sname,width=20).place(x=110,y=40,anchor='nw')
    #部署学号
    tk.Label(regitster,text='学      号',width=15).place(x=10,y=75,anchor='nw')
    tk.Entry(regitster,textvariable=Sno,width=20).place(x=110,y=75,anchor='nw')
    #部署密码
    tk.Label(regitster,text='密      码',width=15).place(x=10,y=110,anchor='nw')
    tk.Entry(regitster,textvariable=Spwd,width=20).place(x=110,y=110,anchor='nw')
    #部署性别
    tk.Label(regitster,text='性      别',width=15).place(x=10,y=145,anchor='nw')
    tk.Entry(regitster,textvariable=Ssex,width=20).place(x=110,y=145,anchor='nw')
    #部署确认/取消组件
    tk.Button(regitster,text='查询',command=lambda :[FU(Sno)],width=10).place(x=30,y=190,anchor='nw')
    tk.Button(regitster,text='修改',command=lambda :ChangeUser(Sname,Sno,Spwd,Ssex),width=10).place(x=170,y=190,anchor='nw')

def ChangeUser_Windos_U():
    #设置窗口
    regitster = tk.Toplevel(root2)
    regitster.title('学生信息修改')
    regitster.geometry('300x220')
    #定义注册信息
    Sname = tk.StringVar()  #学生姓名
    Sno = tk.StringVar()  #学生学号
    Spwd = tk.StringVar()  #学生密码
    Ssex = tk.StringVar()  #学生性别
    #部署姓名
    tk.Label(regitster,text='姓      名',width=15).place(x=10,y=20)
    tk.Entry(regitster,textvariable=Sname,width=20).place(x=110,y=20,anchor='nw')
    #部署学号
    tk.Label(regitster,text='学      号',width=15).place(x=10,y=55,anchor='nw')
    tk.Entry(regitster,textvariable=Sno,width=20).place(x=110,y=55,anchor='nw')
    #部署密码
    tk.Label(regitster,text='密      码',width=15).place(x=10,y=90,anchor='nw')
    tk.Entry(regitster,textvariable=Spwd,width=20).place(x=110,y=90,anchor='nw')
    #部署性别
    tk.Label(regitster,text='性      别',width=15).place(x=10,y=125,anchor='nw')
    tk.Entry(regitster,textvariable=Ssex,width=20).place(x=110,y=125,anchor='nw')
    #部署确认/取消组件
    tk.Button(regitster,text='修改',command=lambda :UChangeUser(Sname,Sno,Spwd,Ssex,regitster),width=10).place(x=100,y=160,anchor='nw')

def UChangeUser(Sname,Sno1,Spwd,Ssex,reg):
    Sname = str(Sname.get())
    Sno = str(Sno1.get())
    Spwd = str(Spwd.get())
    Ssex = str(Ssex.get())
    cur.execute('UPDATE StudentInfo SET Sname = %s,Sno = %s,Spwd = %s,Ssex = %s  WHERE Sno = %s', (Sname,Sno,Spwd,Ssex,Sno))
    con.commit()
    messagebox.showinfo(title='提示',message='修改成功！')
    UFU()
    reg.destroy()

#管理员修改学生信息
def ChangeUser(Sname,Sno1,Spwd,Ssex):
    Sname = str(Sname.get())
    Sno = str(Sno1.get())
    Spwd = str(Spwd.get())
    Ssex = str(Ssex.get())
    if Change_result == 1:
        cur.execute('UPDATE StudentInfo SET Sname = %s,Sno = %s,Spwd = %s,Ssex = %s  WHERE Sno = %s', (Sname,Sno,Spwd,Ssex,Sno))
        con.commit()
        messagebox.showinfo(title='提示',message='修改成功！')
        FU(Sno1)
    else:
        messagebox.showerror(title='警告',message='未查询到该生信息，修改失败！')

#管理员用查找学生信息
def FindUser():
    find = tk.Toplevel(root1)
    find.title('查找学生')
    find.geometry('400x150')
    Sno = tk.StringVar()
    tk.Label(find, text="学   号：", font=('Arial', 10), width=15).place(x=15, y=20, anchor='nw')
    tk.Entry(find, show=None, font=('宋体', 15), textvariable=Sno, width=20).place(x=130, y=20, anchor='nw')
    tk.Button(find, text='确认', bg='silver', command=lambda :FU(Sno),font=('Arial', 12),width=10).place(x=130, y=70, anchor='nw')

#管理员显示体温异常学生信息
def FindWarnUser():
    str_out = ""
    cur.execute('SELECT * FROM Temp')
    results = cur.fetchall()
    str_out +="体温异常学生信息如下：\n"
    str_out += ("{:^}".format("编号") + ' ' * (4 - Len_Str("编号")) +
                "{:^}".format("学号") + ' ' * (19 - Len_Str("学号")) +
                "{:^}".format(str("体温")) + ' ' * (20 - Len_Str(str("体温"))) +
                "{:^}".format(str("日期")) + ' ' * (
                        20 - Len_Str(str("日期"))) +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += ("{:^}".format(result1[0]) + ' ' * (4 - Len_Str(result1[0])) +
                "{:^}".format(result1[1]) + ' ' * (17 - Len_Str(result1[1])) +
                "{:^}".format(str(result1[2])) + ' ' * (20 - Len_Str(str(result1[2]))) +
                "{:^}".format(str(result1[3])) + ' ' * (
                        20 - Len_Str(str(result1[3]))) +
                "\n")
    result.set(str_out)

#学生用查找学生信息
def UFU():
    str_out = ""
    cur.execute('SELECT * FROM StudentInfo where Sno = %s',Sno_global)
    results = cur.fetchall()
    if results:
        global Change_result
        Change_result = 1
        for result1 in results:
            result1 = list(result1)  # 元组转化为列表
            for res in range(len(result1)):
                if isinstance(result1[res], str):
                    result1[res] = result1[res].replace(' ', '')  # 解决空格问题
            result1 = tuple(result1)  # 列表再.转换为元组
        str_out += "姓名："+result1[0]+"\n"
        str_out += "学号："+result1[1]+"\n"
        str_out += "密码："+result1[2]+"\n"
        str_out += "性别："+result1[3]+"\n"
        str_out += "所在自习室编号："+str(result1[4])+"\n"
        str_out += "开始学习时间："+str(result1[5])+"\n"
        str_out += "结束学习时间："+str(result1[6])+"\n"
        str_out += "学习总时间："+str(result1[7])+"\n"
        str_out += "体温："+str(result1[8])+"\n"
    else:
        str_out = "无该生信息"
    resultU.set(str_out)


def FU(Sno):
    str_out = ""
    Sno = str(Sno.get())
    cur.execute('SELECT * FROM StudentInfo where Sno = %s',Sno)
    results = cur.fetchall()
    if results:
        global Change_result
        Change_result = 1
        for result1 in results:
            result1 = list(result1)  # 元组转化为列表
            for res in range(len(result1)):
                if isinstance(result1[res], str):
                    result1[res] = result1[res].replace(' ', '')  # 解决空格问题
            result1 = tuple(result1)  # 列表再.转换为元组
        str_out += "姓名："+result1[0]+"\n"
        str_out += "学号："+result1[1]+"\n"
        str_out += "密码："+result1[2]+"\n"
        str_out += "性别："+result1[3]+"\n"
        str_out += "所在自习室编号："+str(result1[4])+"\n"
        str_out += "开始学习时间："+str(result1[5])+"\n"
        str_out += "结束学习时间："+str(result1[6])+"\n"
        str_out += "学习总时间："+str(result1[7])+"\n"
        str_out += "体温："+str(result1[8])+"\n"
    else:
        str_out = "无该生信息"
    result.set(str_out)

def StudyStar(Star,windows):
    global startime
    str_out = ""
    time = str(Star.get())
    cur.execute('UPDATE StudentInfo SET Sstar = %s WHERE Sno = %s', (time,Sno_global))
    cur2.execute('UPDATE StudentInfo SET Send = NULL ,Sall = NULL WHERE Sno = %s', (Sno_global))
    con.commit()
    messagebox.showinfo(title='成功！',message='记录成功！')
    startime = 1
    cur1.execute('SELECT * FROM StudentInfo where Sno = %s',Sno_global)
    results = cur1.fetchall()
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace(' ', '')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
    str_out += "更新后信息如下：\n"
    str_out += "姓名：" + result1[0] + "\n"
    str_out += "学号：" + result1[1] + "\n"
    str_out += "密码：" + result1[2] + "\n"
    str_out += "性别：" + result1[3] + "\n"
    str_out += "所在自习室编号：" + str(result1[4]) + "\n"
    str_out += "开始学习时间：" + str(result1[5]) + "\n"
    str_out += "结束学习时间：" + str(result1[6]) + "\n"
    str_out += "学习总时间：" + str(result1[7]) + "\n"
    str_out += "体温：" + str(result1[8]) + "\n"
    resultU.set(str_out)
    windows.destroy()

def StudyStarWindows():
    windows = tk.Toplevel(root2)
    Star = tk.StringVar()
    windows.title('学习时间录入')
    windows.geometry('400x160')
    tk.Label(windows,text='记录格式提示：XX(月份)-XX(日)-XX(时):XX(分)',font=('楷体',12)).place(x=10, y=10)
    tk.Label(windows,text='记录格式提示：时:分之间用英文 : ',fg='red',font=('楷体',12)).place(x=10, y=40)
    tk.Label(windows,text='开始学习时间：',font=('楷体',12)).place(x=60,y=75)
    tk.Entry(windows,textvariable=Star,font=('楷体',12)).place(x=170,y=75)
    tk.Button(windows,text='记录',font=('楷体',12),width=10,command=lambda :StudyStar(Star,windows)).place(x=30,y=115)
    tk.Button(windows,text='取消',font=('楷体',12),width=10,command=lambda :windows.destroy()).place(x=280,y=115)

def StudyEndWindows():
    if startime == 1:
        windows = tk.Toplevel(root2)
        End = tk.StringVar()
        windows.title('结束时间录入')
        windows.geometry('400x150')
        tk.Label(windows,text='记录格式提示：XX(月份)-XX(日)-XX(时):XX(分)',font=('楷体',12)).place(x=10, y=10)
        tk.Label(windows,text='请注意：时:分之间用英文！',fg='red',font=('楷体',15)).place(x=75,y=37)
        tk.Label(windows,text='结束学习时间：',font=('楷体',12)).place(x=60,y=75)
        tk.Entry(windows,textvariable=End,font=('楷体',12)).place(x=170,y=75)
        tk.Button(windows,text='记录',font=('楷体',12),width=10,command=lambda :StudyEnd(End,windows)).place(x=30,y=105)
        tk.Button(windows,text='取消',font=('楷体',12),width=10,command=lambda :windows.destroy()).place(x=280,y=105)
    else:
        messagebox.showinfo(title='提示', message='请记录学习时间后离开！')

def StudyEnd(end,windows):
    global FreeRoom
    str_out = ""
    time = str(end.get())
    FreeRoom += 1
    cur.execute('UPDATE StudentInfo SET Send = %s WHERE Sno = %s', (time,Sno_global))
    cur3.execute('UPDATE StudentInfo SET Rno = NULL WHERE Sno = %s',  Sno_global)
    cur4.execute('UPDATE Room SET Rfree = %s WHERE Rno = %s',  (FreeRoom,URoom))
    con.commit()
    cur2.execute('SELECT * FROM StudentInfo where Sno = %s',Sno_global)
    results = cur2.fetchall()
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace(' ', '')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
    s = result1[5]
    e = result1[6]
    s_date = datetime.datetime.strptime(s, '%m-%d-%H:%M')
    e_date = datetime.datetime.strptime(e, '%m-%d-%H:%M')
    sum_month = e_date.month - s_date.month
    sum_day = e_date.day - s_date.day
    sum_hour = e_date.hour - s_date.hour
    sum_minute = e_date.minute - s_date.minute
    Sall = str(str(sum_month)+'月'+str(sum_day)+"日"+str(sum_hour)+"时"+str(sum_minute)+"分")
    messagebox.showinfo(title='成功！',message='签退成功！')
    str_out += "自习室签退成功，本次共学习时长为：\n"
    str_out += Sall+"\n"
    str_out += "自习室欢迎您再次学习！\n"
    str_out += "窗口将在5秒后自动关闭！"
    cur1.execute('UPDATE StudentInfo SET Sall = %s WHERE Sno = %s', (Sall,Sno_global))
    con.commit()
    resultU.set(str_out)
    windows.destroy()
    root2.after(5000,root2.destroy)

def ShowRooms():
    str_out = ""
    cur.execute('SELECT * FROM Room')
    results = cur.fetchall()
    str_out +="自习室信息如下：\n"
    str_out += ("{:^}".format("自习室编号") + ' ' * (15 - Len_Str("自习室编号")) +
                "{:^}".format("可容纳人数") + ' ' * (15 - Len_Str("可容纳人数")) +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += ("{:^}".format(str(result1[0])) + ' ' * (15 - Len_Str(str(result1[0]))) +
                "{:^}".format(str(result1[1])) + ' ' * (15 - Len_Str(str(result1[1]))) +
                "\n")
    result.set(str_out)

def ShowAdmin():
    str_out = ""
    cur.execute('SELECT * FROM Admin')
    results = cur.fetchall()
    str_out +="管理员信息如下：\n"
    str_out += ("{:^}".format("管理员姓名") + ' ' * (22 - Len_Str("管理员姓名")) +
                "{:^}".format("管理员工号") + ' ' * (22 - Len_Str("管理员工号")) +
                "{:^}".format("密码") + ' ' * (25 - Len_Str("密码")) +
                "{:^}".format("性别") + ' ' * (25 - Len_Str("性别")) +
                "{:^}".format("手机号") + ' ' * (18 - Len_Str("手机号")) +
                "\n")
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace('  ', ' ')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
        str_out += ("{:^}".format(result1[0]) + ' ' * (22 - Len_Str(result1[0])) +
                "{:^}".format(result1[1]) + ' ' * (20 - Len_Str(result1[1])) +
                "{:^}".format(result1[2]) + ' ' * (
                        25 - Len_Str(result1[2])) +
                "{:^}".format(str(result1[3])) + ' ' * (25 - Len_Str(str(result1[3]))) +
                "{:^}".format(result1[4]) + ' ' * (
                        15 - Len_Str(result1[4])) +
                "\n")
    result.set(str_out)

def DeleteGoods_Windos():
    windows = tk.Toplevel(root1)
    id = tk.StringVar()
    windows.title('物品删除')
    windows.geometry('300x120')
    tk.Label(windows,text='请输入物品编号',font=('楷体',12)).place(x=10,y=25)
    tk.Entry(windows,textvariable=id).place(x=140,y=25)
    tk.Button(windows,text='查询',command=lambda :FG(id)).place(x=70,y=70)
    tk.Button(windows,text='删除',command=lambda :DG(id,windows)).place(x=170,y=70)

def FG(id):
    str_out = ""
    id = str(id.get())
    cur.execute('select * from Goods where Gno = %s',id)
    results = cur.fetchall()
    for result1 in results:
        result1 = list(result1)  # 元组转化为列表
        for res in range(len(result1)):
            if isinstance(result1[res], str):
                result1[res] = result1[res].replace(' ', '')  # 解决空格问题
        result1 = tuple(result1)  # 列表再.转换为元组
    if results:
        str_out += "物品查询成功，物品信息如下：\n"
        str_out += "物品编号："+result1[0]+"\n"
        str_out += "物品名称："+result1[1]+'\n'
        str_out += "所属自习室："+result1[2]+"\n"
        result.set(str_out)
        global  DeletG
        DeletG = 1
    else:
        str_out += "物品信息不存在\n"
        result.set(str_out)
        messagebox.showerror(title='错误',message='所查询物品不存在！')

def DG(id,windows):
    if DeletG == 1:
        no = str(id.get())
        cur.execute('delete from Goods where Gno = %s',no)
        con.commit()
        messagebox.showinfo(title='成功',message='删除成功！')
        windows.destroy()
        ShowAllGoods()
    else:
        messagebox.showerror(title='错误',message='请查询存在后在删除！')

if __name__ == '__main__':
    if(Admin == 0):
        root = tk.Tk()
        ttkbootstrap.Style(theme='sandstone')
        root.title('自习室管理系统')
        root.geometry('380x100')
    #添加管理员登陆组件
        tk.Button(root,text='管理员登录',font=('楷体'),command=lambda :Login_Admin()).place(x=10,y=30)
        tk.Button(root,text='学生注册',font=('楷体'),command=lambda :Register_User()).place(x=140,y=30)
        tk.Button(root,text='学生登录',font=('楷体'),command=lambda :Login_User()).place(x=250,y=30)
        root.mainloop()

    if(Admin == 1):
        root1 = tk.Tk()
        root1.title('管理员管理系统')
        root1.geometry('1400x580')
        # 添加管理员登陆组件
        tk.Button(root1, text='显 示 管 理 员 信 息', bg='silver', font=('Arial', 12), command=lambda :ShowAdmin(), width=20).place(x=50, y=10, anchor='nw')
        tk.Button(root1, text='增 添 物 品 信 息', bg='silver', font=('Arial', 12), command=lambda :GoodsWinidows(), width=20).place(x=50, y=60, anchor='nw')
        tk.Button(root1, text='查 询 学 生 信 息', bg='silver', font=('Arial', 12), command=lambda :FindUser(), width=20).place(x=50, y=110, anchor='nw')
        tk.Button(root1, text='显 示 物 品 信 息 ', bg='silver', font=('Arial', 12), command=lambda :ShowAllGoods(), width=20).place(x=50, y=160, anchor='nw')
        tk.Button(root1, text='显 示 学 生 信 息 ', bg='silver', font=('Arial', 12), command=lambda :ShowAllStudents(), width=20).place(x=50, y=210, anchor='nw')
        tk.Button(root1, text='异 常 学 生 信 息', bg='silver', font=('Arial', 12), command=lambda :FindWarnUser(), width=20).place(x=50, y=260, anchor='nw')
        tk.Button(root1, text='修 改 学 生 信 息', bg='silver', font=('Arial', 12), command=lambda :ChangeUser_Windos(), width=20).place(x=50, y=310, anchor='nw')
        tk.Button(root1, text='损 坏 物 品 信 息', bg='silver', font=('Arial', 12), command=lambda :SearchBadGoods(), width=20).place(x=50, y=360, anchor='nw')
        tk.Button(root1, text=' 一键修复损坏物品 ', bg='silver', font=('Arial', 12), command=lambda :RestoreGoods(), width=20).place(x=50, y=410, anchor='nw')
        tk.Button(root1, text='  显示自习室信息  ', bg='silver', font=('Arial', 12), command=lambda :ShowRooms(), width=20).place(x=50, y=460, anchor='nw')
        tk.Button(root1, text='删 除 物 品 信 息', bg='silver', font=('Arial', 12), command=lambda :DeleteGoods_Windos(), width=20).place(x=50, y=510, anchor='nw')


        result = tk.StringVar()
        result.set(">>>欢迎使用自习室管理系统<<<\n   Edition:  V0.0.1   \n   @Author:  陈嘉乐   李梦瑶   付家正   高梦伟\nAdvisor:  王丽丽")
        Show_result = tk.Label(root1, bg="white", fg="black", font=("楷体", 13), bd='0', anchor='nw', textvariable=result)
        Show_result.place(x="300", y="50", width="1100", height="400")
        root1.mainloop()

    if(Admin == 2):
        root2 = tk.Tk()
        root2.title('自习室信息管理系统')
        root2.geometry('800x300')
        messagebox.showinfo(title='欢迎！',message='欢迎使用自习室管理系统！V0.0.1')

        #
        tk.Button(root2, text='记 录 学 习 时 间', bg='silver', font=('Arial', 12), command=lambda :StudyStarWindows(), width=20).place(x=50, y=20, anchor='nw')
        tk.Button(root2, text='查 询 个 人 信 息', bg='silver', font=('Arial', 12), command=lambda :UFU(), width=20).place(x=50, y=70, anchor='nw')
        tk.Button(root2, text='个 人 信 息 维 护', bg='silver', font=('Arial', 12), command=lambda :ChangeUser_Windos_U(), width=20).place(x=50, y=120, anchor='nw')
        tk.Button(root2, text='损 坏 物 品 报 备', bg='silver', font=('Arial', 12), command=lambda :GoodsReportWindows(), width=20).place(x=50, y=170, anchor='nw')
        tk.Button(root2, text='一   键   离   开', bg='silver', font=('Arial', 12), command=lambda :StudyEndWindows(), width=20).place(x=50, y=220, anchor='nw')


        # 添加管理员登陆组件
        resultU = tk.StringVar()
        resultU.set(">>>欢迎使用自习室管理系统<<<\n   Edition:  V0.0.1   \n   @Author:  陈嘉乐   李梦瑶   付家正   高梦伟\nAdvisor:  王丽丽")
        Show_result = tk.Label(root2, bg="white", fg="black", font=("楷体", 13), bd='0', anchor='nw', textvariable=resultU)
        Show_result.place(x="270", y="20", width="450", height="250")
        root2.mainloop()