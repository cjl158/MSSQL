USE [master]
GO
/****** Object:  Database [MSSQL]    Script Date: 2022/12/16 14:38:54 ******/
CREATE DATABASE [MSSQL]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MSSQL', FILENAME = N'E:\sql\MSSQL15.MSSQLSERVER\MSSQL\DATA\MSSQL.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MSSQL_log', FILENAME = N'E:\sql\MSSQL15.MSSQLSERVER\MSSQL\DATA\MSSQL_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MSSQL] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MSSQL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MSSQL] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MSSQL] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MSSQL] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MSSQL] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MSSQL] SET ARITHABORT OFF 
GO
ALTER DATABASE [MSSQL] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MSSQL] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MSSQL] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MSSQL] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MSSQL] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MSSQL] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MSSQL] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MSSQL] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MSSQL] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MSSQL] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MSSQL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MSSQL] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MSSQL] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MSSQL] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MSSQL] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MSSQL] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MSSQL] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MSSQL] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MSSQL] SET  MULTI_USER 
GO
ALTER DATABASE [MSSQL] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MSSQL] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MSSQL] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MSSQL] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MSSQL] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MSSQL] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [MSSQL] SET QUERY_STORE = OFF
GO
USE [MSSQL]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[name] [nvarchar](50) NULL,
	[id] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[sex] [nvarchar](50) NULL,
	[number] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Goods]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goods](
	[Gno] [nvarchar](50) NULL,
	[Gname] [nvarchar](50) NULL,
	[Rno] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Repair]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repair](
	[Gno] [nvarchar](50) NULL,
	[Sno] [nvarchar](50) NULL,
	[id] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[Rno] [nvarchar](50) NOT NULL,
	[Rfree] [int] NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[Rno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentInfo]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentInfo](
	[Sname] [nvarchar](50) NOT NULL,
	[Sno] [nvarchar](50) NOT NULL,
	[Spwd] [nvarchar](50) NULL,
	[Ssex] [nvarchar](50) NULL,
	[Rno] [nvarchar](50) NULL,
	[Sstar] [nvarchar](50) NULL,
	[Send] [nvarchar](50) NULL,
	[Sall] [nvarchar](50) NULL,
	[Stemp] [float] NULL,
 CONSTRAINT [PK_StudentInfo] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Temp]    Script Date: 2022/12/16 14:38:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp](
	[No] [nvarchar](50) NOT NULL,
	[Sno] [nvarchar](50) NOT NULL,
	[Stemp] [nvarchar](50) NULL,
	[Sdate] [nvarchar](50) NULL,
 CONSTRAINT [PK_Temp] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Admin] ([name], [id], [password], [sex], [number]) VALUES (N'陈嘉乐', N'admin', N'admin', N'男', N'15092259801')
GO
INSERT [dbo].[Goods] ([Gno], [Gname], [Rno]) VALUES (N'1001', N'桌子', N'1')
INSERT [dbo].[Goods] ([Gno], [Gname], [Rno]) VALUES (N'2001', N'椅子', N'1')
GO
INSERT [dbo].[Repair] ([Gno], [Sno], [id], [state]) VALUES (N'1001', N'202101007012', NULL, N'异常')
GO
INSERT [dbo].[Room] ([Rno], [Rfree]) VALUES (N'1', 50)
INSERT [dbo].[Room] ([Rno], [Rfree]) VALUES (N'2', 0)
INSERT [dbo].[Room] ([Rno], [Rfree]) VALUES (N'3', 50)
GO
INSERT [dbo].[StudentInfo] ([Sname], [Sno], [Spwd], [Ssex], [Rno], [Sstar], [Send], [Sall], [Stemp]) VALUES (N'陈嘉乐', N'202101007012', N'123456', N'男', N'1', NULL, NULL, NULL, 36.5)
GO
USE [master]
GO
ALTER DATABASE [MSSQL] SET  READ_WRITE 
GO
